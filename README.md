### PLOP templates

Use these commands to generate code:

- `npm run g` or `npm run g ui component-name`

### TODO

- Тесты содержат вопросы, вопросы могут быть разных типов: с вариантами ответа, произвольный текст
- Тесты можно группировать. Пример - набор тестов для изучения всех времён в английском языке
- Тесты могут быть привытными и публичными. Публичными тестами можно делиться в соцсетях, для них будет отображаться превью
- Можно устанавливать Telegram напоминания о повторном прохождении тестов. 
- 

- Напоминания по кривой забывания Эбингауза
- Email напоминания
- Тип вопросов - установить соответствия
