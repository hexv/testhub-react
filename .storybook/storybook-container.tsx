import React, { ReactNode } from 'react';
import '../src/css/index.scss';
import './storybook-container.scss';

type Props = {
  children: ReactNode;
};

export const StorybookContainer = (props: Props) => {
  return <div className='storybookContainer'>{props.children}</div>;
};
