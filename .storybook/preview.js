import { addParameters } from '@storybook/react';

addParameters({
  backgrounds: [
    { name: 'default', value: '#e8e6f2', default: true },
    { name: 'white', value: '#fff' }
  ]
});
