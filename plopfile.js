module.exports = plop => {
  plop.setGenerator('ui', {
    description: 'Create a UI component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?'
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'src/ui/{{kebabCase name}}/{{kebabCase name}}.tsx',
        templateFile: 'plop-templates/ui-component.tsx.hbs',
      },
      {
        type: 'add',
        path: 'src/ui/{{kebabCase name}}/{{kebabCase name}}.module.scss',
        templateFile: 'plop-templates/ui-component.module.scss.hbs',
      },
      {
        type: 'add',
        path: 'src/ui/{{kebabCase name}}/{{kebabCase name}}.stories.tsx',
        templateFile: 'plop-templates/ui-component.stories.tsx.hbs',
      },
    ],
  });
};
