import { Test } from '../../model/test';
import { Id } from '../../model/id';
import { createMockTest } from './create-mock-test';

export class TestApiMock {
  save = jest.fn(() => Promise.resolve());

  private testStorage: { [key in Id]: Test } = {
    '1': createMockTest()
  };

  getOne(id: Id) {
    return Promise.resolve(this.testStorage[id]);
  }
}
