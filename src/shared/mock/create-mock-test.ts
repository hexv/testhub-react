import { Test } from '../../model/test';
import * as firebase from 'firebase/app';
import 'firebase/firestore';

export const createMockTest = (): Test => {
  return {
    id: '1',
    createdAt: firebase.firestore.Timestamp.now(),
    title: 'math test',
    description: 'Basic math test',
    tags: ['math', 'algebra'],
    configShowCorrectAnswer: false,
    configIsPrivate: false,
    configShuffleQuestionsAnswers: false,
    questions: [
      {
        id: '21',
        title: '2+2?',
        type: 'single',
        answerText: '4',
        answerVariants: [],
        orderIndex: 0
      },
      {
        id: '22',
        title: '3+3?',
        type: 'single',
        answerText: '6',
        answerVariants: [],
        orderIndex: 1
      },
      {
        id: '23',
        title: '4+4?',
        type: 'multiple',
        answerText: '',
        orderIndex: 2,
        answerVariants: [
          { id: '31', text: '1', isCorrect: false, orderIndex: 0 },
          {
            id: '32',
            text: '2',
            isCorrect: false,
            orderIndex: 1
          },
          {
            id: '33',
            text: '8',
            isCorrect: true,
            orderIndex: 2
          }
        ]
      }
    ]
  };
};
