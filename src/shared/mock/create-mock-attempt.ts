import { createMockTest } from './create-mock-test';
import { createAttempt } from '../../model/attempt';
import { createAnswer } from '../../model/answer';
import { assert } from 'ts-essentials';

export const createMockAttempt = () => {
  const test = createMockTest();
  const questions = test.questions;
  const thirdQuestionCorrectVariant = questions[2].answerVariants.find(
    variant => variant.isCorrect
  );
  assert(thirdQuestionCorrectVariant);
  const attempt = createAttempt(test.id, [
    createAnswer(questions[0].id, questions[0].answerText, true),
    createAnswer(questions[1].id, 'incorrect', false),
    createAnswer(questions[2].id, thirdQuestionCorrectVariant.id, false)
  ]);

  return { test, attempt };
};
