import { useCallback, useState } from 'react';

export const useToggle = (initialValue = false) => {
  const [isOn, set] = useState(initialValue);
  const toggle = useCallback(() => set(!isOn), [isOn]);
  return [isOn, toggle] as const;
};
