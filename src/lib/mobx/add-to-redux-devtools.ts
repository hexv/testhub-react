const spy = require('mobx-remotedev/lib/spy').default;

export const addToReduxDevtools = (store: object) => {
  spy(store, { global: true, name: store.constructor.name });
};
