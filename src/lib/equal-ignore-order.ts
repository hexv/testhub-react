export const equalIgnoreOrder = <T>(left: T[], right: T[]): boolean => {
  return (
    left.length === right.length &&
    left.every(item => right.indexOf(item) > -1) &&
    right.every(item => left.indexOf(item) > -1)
  );
};
