import { move, resetOrder } from './sorting';

describe('sorting', () => {
  it('should move an item by index', () => {
    const moved = move(
      [
        { name: 'Fred', orderIndex: 0 },
        { name: 'Barney', orderIndex: 1 },
        { name: 'Wilma', orderIndex: 2 },
        { name: 'Betty', orderIndex: 3 }
      ],
      2,
      0
    );
    expect(moved).toStrictEqual([
      { name: 'Wilma', orderIndex: 2 },
      { name: 'Fred', orderIndex: 0 },
      { name: 'Barney', orderIndex: 1 },
      { name: 'Betty', orderIndex: 3 }
    ]);
  });

  it('should reset indices after sorting', () => {
    const reset = resetOrder([
      { name: 'Wilma', orderIndex: 2 },
      { name: 'Fred', orderIndex: 0 },
      { name: 'Barney', orderIndex: 1 },
      { name: 'Betty', orderIndex: 3 }
    ]);

    expect(reset).toStrictEqual([
      { name: 'Wilma', orderIndex: 0 },
      { name: 'Fred', orderIndex: 1 },
      { name: 'Barney', orderIndex: 2 },
      { name: 'Betty', orderIndex: 3 }
    ]);
  });
});
