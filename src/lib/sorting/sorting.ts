export type Sortable = {
  orderIndex: number;
};

export const move = <T>(array: T[], moveIndex: number, toIndex: number): T[] => {
  const itemRemovedArray = [
    ...array.slice(0, moveIndex),
    ...array.slice(moveIndex + 1, array.length)
  ];
  return [
    ...itemRemovedArray.slice(0, toIndex),
    array[moveIndex],
    ...itemRemovedArray.slice(toIndex, itemRemovedArray.length)
  ];
};

export const resetOrder = <T extends Sortable>(sortables: T[]): T[] => {
  return sortables.map((sortable: Sortable, i) => {
    return { ...sortable, orderIndex: i } as T;
  });
};
