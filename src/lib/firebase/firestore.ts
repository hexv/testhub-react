import { firestore } from 'firebase/app';

export class Firestore<EntityMap extends { [key in string]: { id: string } }> {
  private firestore: firestore.Firestore;

  constructor(firestore: firestore.Firestore) {
    this.firestore = firestore;
  }

  async getAll<Key extends keyof EntityMap & string>(key: Key): Promise<Array<EntityMap[Key]>> {
    const snapshot = await this.firestore.collection(key).get();
    const collection: Array<EntityMap[Key]> = [];
    snapshot.forEach(doc => {
      collection.push(doc.data() as EntityMap[Key]);
    });
    return collection;
  }

  async getOne<Key extends keyof EntityMap & string>(
    key: Key,
    id: string
  ): Promise<EntityMap[Key] | null> {
    // prettier-ignore
    const snapshot = await this.firestore.collection(key).doc(id).get();
    const data = snapshot.data() as EntityMap[Key] | undefined;
    return data ?? null;
  }

  set<Key extends keyof EntityMap & string>(key: Key, entity: EntityMap[Key]): Promise<void> {
    return this.firestore
      .collection(key)
      .doc(entity.id)
      .set(entity);
  }

  update<Key extends keyof EntityMap & string>(
    key: Key,
    entity: Partial<EntityMap[Key]>
  ): Promise<void> {
    return this.firestore
      .collection(key)
      .doc(entity.id)
      .update(entity);
  }

  delete<Key extends keyof EntityMap & string>(key: Key, id: string): Promise<void> {
    return this.firestore
      .collection(key)
      .doc(id)
      .delete();
  }
}
