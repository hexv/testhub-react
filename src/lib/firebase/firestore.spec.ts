import { Firestore } from './firestore';
import { assert, IsExact } from 'conditional-type-checks';
import { Mock } from 'firebase-nightlight';

const createFireStoreMock = () => {
  const mock = new Mock();
  return mock.firestore(mock.initializeApp({}));
};

describe('Firestore', () => {
  it('should infer types correctly', () => {
    type User = { id: string; email: string };
    type Post = { id: string; text: string };
    type UrlEntityMap = {
      user: User;
      post: Post;
    };

    const firestore = new Firestore<UrlEntityMap>(createFireStoreMock());

    const users = firestore.getAll('user');
    assert<IsExact<typeof users, Promise<User[]>>>(true);
    assert<IsExact<typeof users, Promise<Post[]>>>(false);

    const posts = firestore.getAll('post');
    assert<IsExact<typeof posts, Promise<Post[]>>>(true);
    assert<IsExact<typeof posts, Promise<User[]>>>(false);
  });
});
