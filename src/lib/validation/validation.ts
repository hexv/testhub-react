type Validator<T> = (value: T) => string | false;

export const required = (formatError?: () => string) => <T>(value: T) => {
  if (!value) {
    return formatError ? formatError() : 'Value is required';
  }
};

export const minValue = (
  minValue: number,
  formatError: (minValue: number, actualValue?: number | null) => string
) => (value: number | null) => {
  if (value !== null && value < minValue) {
    return formatError ? formatError(minValue, value) : `Value should not be less than ${minValue}`;
  }
};

export const minLength = (
  minLength: number,
  formatError?: (minLength: number, actualValue: number) => string
) => (value: string) => {
  const actualLength = value.length;
  if (value && actualLength < 6) {
    return formatError
      ? formatError(minLength, actualLength)
      : `Value should have at least ${minLength} characters`;
  }
};

export const conditional = <T>(condition: () => boolean, validator: Validator<T>) => {
  if (condition()) {
    return validator;
  }
};

export const composeValidators = <T>(...validators: Validator<T>[]) => (value: T) => {
  return validators.reduce<false | string>((error, validator) => error || validator(value), false);
};
