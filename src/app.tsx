import React from 'react';
import { hot } from 'react-hot-loader/root';
import { Main } from './pages/main/main';
import { Router, Route, Switch } from 'react-router-dom';
import { TestsPage } from './pages/tests/tests-page';
import { TestPage } from './pages/test-edit/test-page';
import { TestPreviewPage } from './pages/test-preview/test-preview-page';
import { AttemptPage } from './pages/attempt/attempt-page';
import { useStore } from './store/use-store';

export const App = hot(() => {
  const { routerStore } = useStore();

  return (
    <Router history={routerStore.history}>
      <Switch>
        <Route path={'/'} exact component={Main} />
        <Route path={'/tests'} component={TestsPage} />
        <Route path={'/test'} component={TestPage} />
        <Route path={'/test/:id'} component={TestPreviewPage} />
        <Route path={'/test/:id/edit'} component={TestPage} />
        <Route path={'/attempt'} component={AttemptPage} />
      </Switch>
    </Router>
  );
});
