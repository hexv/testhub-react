import React, { useEffect, useRef } from 'react';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../store/use-store';
import { Link } from 'react-router-dom';

export const TestsPage = observer(() => {
  const { createTestsPageStore } = useStore();
  const { current: testsPageStore } = useRef(createTestsPageStore());

  useEffect(() => {
    testsPageStore.load();
  }, [testsPageStore]);

  if (testsPageStore.isLoading) {
    return <>Loading...</>;
  }

  return (
    <>
      <h1>Tests</h1>
      <Link to={`/`}>{'<- Back'}</Link>
      <ul>
        {testsPageStore.tests.map((test, i) => (
          <li key={i}>
            <Link to={`/test/${test.id}`}>{test.title}</Link>
          </li>
        ))}
      </ul>
    </>
  );
});
