import React from 'react';
import { observer } from 'mobx-react-lite';

type Props = {};

export const TestPreviewPage = observer((props: Props) => {
  return <h1>Test preview</h1>;
});
