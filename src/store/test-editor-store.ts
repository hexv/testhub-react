import { id, Id } from '../model/id';
import { action, computed, observable } from 'mobx';
import { createEmptyTest, Test, validateTest } from '../model/test';
import { TestApi } from '../api/test-api';
import { MessageService } from '../shared/message-service';
import { createEmptyQuestion, Question, QuestionType, validateQuestion } from '../model/question';
import { createEmptyVariant, Variant } from '../model/variant';
import { cloneDeep, orderBy } from 'lodash';
import { move, resetOrder } from '../lib/sorting/sorting';
import { RouterStore } from 'mobx-react-router';
import { computedFn } from 'mobx-utils';
import { assert } from 'ts-essentials';

export class TestEditorStore {
  @observable test?: Test;
  @observable isTestLoading = false;
  @observable isTestSaving = false;
  private collapsedQuestions = new Map<Id, boolean>();

  constructor(
    private testApi: TestApi,
    private messageService: MessageService,
    private router: RouterStore
  ) {}

  @action loadTest(id: Id): void {
    this.isTestLoading = true;
    this.testApi
      .getOne(id)
      .then((test: Test) => (this.test = test))
      .finally(() => (this.isTestLoading = false));
  }

  @action editTest<Key extends keyof Test>(key: Key, value: Test[Key]): void {
    assert(this.test);
    this.test[key] = value;
  }

  @action addTag(tag: string): void {
    assert(this.test);
    this.test.tags.push(tag);
  }

  @action removeTag(tag: string): void {
    assert(this.test);
    this.test.tags = this.test.tags.filter(t => t !== tag);
  }

  @action createEmptyTest(): void {
    this.test = createEmptyTest();
  }

  @action addEmptyQuestion(): void {
    assert(this.test);
    this.test.questions.push(createEmptyQuestion(this.test.questions.length + 1));
  }

  @action editQuestion<Key extends keyof Question>(
    questionId: Id,
    key: Key,
    value: Question[Key]
  ): void {
    const question = this.getQuestion(questionId);
    question[key] = value;
  }

  @action switchQuestionType(questionId: Id, type: QuestionType): void {
    const question = this.getQuestion(questionId);
    if (type === 'multiple' && question.answerVariants.length === 0) {
      this.addEmptyVariant(questionId);
    }
    question.type = type;
  }

  @action removeQuestion(questionId: Id): void {
    assert(this.test);
    this.test.questions = this.test.questions.filter(q => q.id !== questionId);
  }

  @action duplicateQuestion(questionId: Id): void {
    assert(this.test);
    this.test.questions.push(
      cloneDeep({
        ...this.getQuestion(questionId),
        id: id(),
        orderIndex: this.test.questions.length
      })
    );
  }

  @computed get firstQuestionId(): Id | null {
    if (!this.test || !this.test.questions[0]) {
      return null;
    }
    return this.test.questions[0].id;
  }

  @computed get lastQuestionId(): Id | null {
    if (!this.test || !this.test.questions.length) {
      return null;
    }
    const questions = this.test.questions;
    return questions[questions.length - 1].id;
  }

  @action addEmptyVariant(questionId: Id): void {
    const question = this.getQuestion(questionId);
    const variant = createEmptyVariant(question.answerVariants.length + 1);
    question.answerVariants.push(variant);
  }

  @action removeVariant(questionId: Id, variantId: Id): void {
    const question = this.getQuestion(questionId);
    question.answerVariants = question.answerVariants.filter(v => v.id !== variantId);
  }

  @action duplicateVariant(questionId: Id, variantId: Id): void {
    const question = this.getQuestion(questionId);
    const variant = question.answerVariants.find(v => v.id === variantId);
    assert(variant, `Variant ${variantId} not found`);
    question.answerVariants.push(
      cloneDeep({
        ...variant,
        id: id()
      })
    );
  }

  @action editVariant<Key extends keyof Variant>(
    questionId: Id,
    variantId: Id,
    key: Key,
    value: Variant[Key]
  ): void {
    const question = this.getQuestion(questionId);
    const variant = question.answerVariants.find(v => v.id === variantId);
    assert(variant, `Variant ${variantId} not found`);
    variant[key] = value;
  }

  @action submit(): void {
    assert(this.test);
    this.isTestSaving = true;

    this.testApi
      .save(this.test)
      .then(() => this.messageService.success('Тест успешно сохранён'))
      .finally(() => (this.isTestSaving = false));
  }

  @action removeTest(): void {
    assert(this.test);
    this.testApi.remove(this.test.id).then(() => {
      this.messageService.success('Тест успешно удалён');
      this.router.push('/tests');
    });
  }

  getQuestion(id: Id): Question {
    assert(this.test, `Test is not loaded`);
    const question = this.test.questions.find(q => q.id === id);
    assert(question);
    return question;
  }

  @action moveQuestionFromTo(previousIndex: number, currentIndex: number): void {
    assert(this.test);
    this.test.questions = resetOrder(move(this.test.questions, previousIndex, currentIndex));
  }

  @action moveVariantFromTo(questionId: Id, previousIndex: number, currentIndex: number): void {
    assert(this.test);
    const question = this.getQuestion(questionId);
    question.answerVariants = resetOrder(
      move(question.answerVariants, previousIndex, currentIndex)
    );
  }

  @computed get testErrors() {
    if (!this.test) {
      return null;
    }
    return validateTest(this.test);
  }

  questionErrors = computedFn(
    function(this: TestEditorStore, questionId: Id) {
      if (!this.test) {
        return null;
      }
      return validateQuestion(this.getQuestion(questionId));
    }.bind(this)
  );

  @action collapseAll(): void {
    assert(this.test);
    this.test.questions.forEach(question => {
      this.collapsedQuestions.set(question.id, true);
    });
  }

  @action expandAll(): void {
    this.collapsedQuestions.clear();
  }

  @action toggleExpanded(id: Id): void {
    this.collapsedQuestions.set(id, !this.collapsedQuestions.get(id));
  }

  @computed get questionsSorted() {
    assert(this.test);
    return orderBy(this.test.questions, ['orderIndex']);
  }

  isExpanded(id: Id) {
    return !this.collapsedQuestions.get(id);
  }
}
