import React from 'react';
import { RootStore } from './create-stores';
import { assert } from 'ts-essentials';

export const StoreContext = React.createContext<RootStore | null>(null);

export const useStore = () => {
  const context = React.useContext(StoreContext);
  assert(context, 'RootStore cannot be null, please add a context provider');
  return context;
};
