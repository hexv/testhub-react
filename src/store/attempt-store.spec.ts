import { AuthStore } from './auth-store';
import { id, Id } from '../model/id';
import { Test } from '../model/test';
import { createMockTest } from '../shared/mock/create-mock-test';
import { Attempt } from '../model/attempt';
import { firestore } from 'firebase/app';
import { TestApiMock } from '../shared/mock/test-api-mock';
import { AttemptStore } from './attempt-store';
import { when } from 'mobx';
import { RouterStore } from 'mobx-react-router';
import { assert } from 'ts-essentials';

const authServiceMock = {
  currentUser: {
    id: '123',
    email: 'test@test.com'
  }
} as AuthStore;

class AttemptApiMock {
  private testStorage: Record<Id, Test> = {
    '1': createMockTest()
  };

  start(testId: string, userId: string): Promise<Attempt & { test: Test }> {
    const test = this.testStorage[testId];
    return Promise.resolve({
      id: id(),
      createdAt: firestore.Timestamp.now(),
      finishedAt: null,
      testId: test.id,
      answers: [],
      test
    });
  }

  save() {
    return Promise.resolve();
  }
}

const createAttemptStore = () => {
  const routerStoreMock = ({ push: jest.fn() } as any) as RouterStore;
  return new AttemptStore(
    new AttemptApiMock() as any,
    new TestApiMock() as any,
    authServiceMock,
    routerStoreMock
  );
};

describe('AttemptService', () => {
  it('is empty by default', () => {
    const attemptStore = createAttemptStore();

    expect(attemptStore.isAttemptLoading).toBeFalsy();
    expect(attemptStore.attempt).toBeFalsy();
    expect(attemptStore.isCompleted).toBeFalsy();
    expect(attemptStore.isPrevDisabled).toBeTruthy();
    expect(attemptStore.currentAnswerText).toBeFalsy();
    expect(attemptStore.currentAnswerVariants.length).toBe(0);
    expect(attemptStore.currentAnswerStatus).toEqual('empty');
  });

  it('allows to start an attempt', async () => {
    const attemptStore = createAttemptStore();
    attemptStore.startAttempt('1');

    await when(() => !attemptStore.isAttemptLoading);
    expect(attemptStore.isAttemptLoading).toBeFalsy();
    expect(attemptStore.attempt).toBeTruthy();
    assert(attemptStore.attempt);
    expect(attemptStore.attempt.answers.length).toBe(0);
    expect(attemptStore.attempt.createdAt).toBeTruthy();
    expect(attemptStore.attempt.finishedAt).toBeFalsy();
    expect(attemptStore.currentQuestion).toBeTruthy();
  });

  it('allows to navigate back and forward', async () => {
    const attemptStore = createAttemptStore();
    attemptStore.startAttempt('1');

    await when(() => !attemptStore.isAttemptLoading);
    expect(attemptStore.isAttemptLoading).toBeFalsy();
    expect(attemptStore.currentQuestion.id).toEqual('21');
    attemptStore.nextQuestion();
    attemptStore.nextQuestion();
    expect(attemptStore.currentQuestion.id).toEqual('23');
    attemptStore.nextQuestion();
    expect(attemptStore.currentQuestion.id).toEqual('23');
    attemptStore.prevQuestion();
    attemptStore.prevQuestion();
    attemptStore.prevQuestion();
    expect(attemptStore.currentQuestion.id).toEqual('21');
    expect(attemptStore.isPrevDisabled).toBeTruthy();
  });

  it('allows to give an answer', async () => {
    const attemptStore = createAttemptStore();
    attemptStore.startAttempt('1');

    await when(() => !attemptStore.isAttemptLoading);
    expect(attemptStore.isAttemptLoading).toBeFalsy();
    expect(attemptStore.currentAnswerStatus).toEqual('empty');
    assert(attemptStore.attempt);
    expect(attemptStore.attempt.answers.length).toBe(0);
    attemptStore.editCurrentAnswerText('4');
    expect(attemptStore.currentAnswerStatus).toEqual('edited');
    attemptStore.next();
    expect(attemptStore.currentAnswerStatus).toEqual('given');
    expect(attemptStore.attempt.answers.length).toBe(1);
    expect(attemptStore.attempt.answers[0].isCorrect).toBeTruthy();

    attemptStore.next();
    expect(attemptStore.currentAnswerStatus).toEqual('empty');
    attemptStore.editCurrentAnswerText('6');
    expect(attemptStore.currentAnswerStatus).toEqual('edited');
    attemptStore.next();
    expect(attemptStore.currentAnswerStatus).toEqual('given');
    expect(attemptStore.attempt.answers.length).toBe(2);
    expect(attemptStore.attempt.answers[1].isCorrect).toBeTruthy();

    attemptStore.next();
    expect(attemptStore.currentAnswerStatus).toEqual('empty');
    const currentQuestion = attemptStore.currentQuestion;
    if (currentQuestion.type !== 'multiple') {
      throw new Error('Invalid question type');
    }
    attemptStore.toggleCurrentAnswerVariant(currentQuestion.answerVariants[0].id);
    expect(attemptStore.currentAnswerStatus).toEqual('edited');
    attemptStore.next();
    expect(attemptStore.currentAnswerStatus).toEqual('given');
    expect(attemptStore.attempt.answers[2].isCorrect).toBeFalsy();

    expect(attemptStore.isCompleted).toBeTruthy();
    expect(attemptStore.attempt.finishedAt).toBeTruthy();
  });

  it('allows to update an answer', async () => {
    const attemptStore = createAttemptStore();
    attemptStore.startAttempt('1');

    await when(() => !attemptStore.isAttemptLoading);
    assert(attemptStore.attempt);
    expect(attemptStore.attempt.answers.length).toBe(0);
    expect(attemptStore.currentAnswerStatus).toEqual('empty');
    attemptStore.editCurrentAnswerText('4');
    expect(attemptStore.currentAnswerStatus).toEqual('edited');
    attemptStore.editCurrentAnswerText('5');
    expect(attemptStore.currentAnswerStatus).toEqual('edited');
    attemptStore.next();
    expect(attemptStore.currentAnswerStatus).toEqual('given');
    expect(attemptStore.attempt.answers.length).toBe(1);
    expect(attemptStore.attempt.answers[0].isCorrect).toBeFalsy();
  });

  it('allows to finish attempt', async () => {
    const attemptStore = createAttemptStore();
    attemptStore.startAttempt('1');
    await when(() => !!attemptStore.attempt);

    expect(attemptStore.isCompleted).toBeFalsy();
    attemptStore.editCurrentAnswerText('4');
    attemptStore.next();
    attemptStore.next();

    expect(attemptStore.isCompleted).toBeFalsy();
    attemptStore.editCurrentAnswerText('5');
    attemptStore.next();
    attemptStore.next();

    expect(attemptStore.isCompleted).toBeFalsy();
    attemptStore.editCurrentAnswerVariants(['33']);
    attemptStore.next();
    attemptStore.next();

    assert(attemptStore.attempt);
    expect(attemptStore.isCompleted).toBeTruthy();
    expect(attemptStore.attempt.finishedAt).toBeTruthy();
  });
});
