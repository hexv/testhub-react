import { AttemptStore } from './attempt-store';
import { AttemptApi } from '../api/attempt-api';
import { TestApi } from '../api/test-api';
import { createBrowserHistory } from 'history';
import { AuthStore } from './auth-store';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { Firestore } from '../lib/firebase/firestore';
import { initializeApp } from 'firebase';
import { TestEditorStore } from './test-editor-store';
import { MessageService } from '../shared/message-service';
import { config, UrlEntityMap } from '../shared/app-firestore';
import { TestsPageStore } from './tests-page-store';

const firestore = new Firestore<UrlEntityMap>(initializeApp(config).firestore());
const attemptApi = new AttemptApi();
const testApi = new TestApi();
const messageService = new MessageService();

export const createStores = () => {
  // Global stores
  const authStore = new AuthStore();
  const routerStore = new RouterStore();
  // Routing
  const history = createBrowserHistory();
  syncHistoryWithStore(history, routerStore);

  return {
    authStore,
    routerStore,
    // Factories for local stores
    createAttemptStore: () => new AttemptStore(attemptApi, testApi, authStore, routerStore),
    createTestEditorStore: () => new TestEditorStore(testApi, messageService, routerStore),
    createTestsPageStore: () => new TestsPageStore(firestore)
  };
};

export type RootStore = ReturnType<typeof createStores>;
