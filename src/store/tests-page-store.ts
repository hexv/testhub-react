import { AppFirestore } from '../shared/app-firestore';
import { Test } from '../model/test';
import { action, observable } from 'mobx';

export class TestsPageStore {
  @observable tests: Test[] = [];
  @observable isLoading = false;

  constructor(private firestore: AppFirestore) {}

  @action load() {
    this.isLoading = true;
    this.firestore
      .getAll('tests')
      .then(tests => (this.tests = tests))
      .finally(() => (this.isLoading = false));
  }
}
