import { action, computed, observable } from 'mobx';
import { User } from '../model/user';
import { assert } from 'ts-essentials';

export class AuthStore {
  @observable user: User | null = null;

  @action loadUser() {
    this.user = {
      id: 'gfWuwxn2ZoXJy4rEzLkv',
      email: 'test@test.com'
    };
  }

  @computed get currentUser(): User {
    assert(this.user);
    return this.user;
  }
}
