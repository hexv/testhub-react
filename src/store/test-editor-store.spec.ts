import { MessageService } from '../shared/message-service';
import { TestEditorStore } from './test-editor-store';
import { RouterStore } from 'mobx-react-router';
import { TestApiMock } from '../shared/mock/test-api-mock';
import { autorun, when } from 'mobx';
import { assert } from 'ts-essentials';
import { Id } from '../model/id';

const createTestService = () => {
  const messageServiceMock = {
    success(message: string) {},
    error(message: string) {}
  } as MessageService;
  const routerMock = { push(url: string) {} } as RouterStore;

  return new TestEditorStore(new TestApiMock() as any, messageServiceMock, routerMock);
};

const prepareTestService = async (testId: Id) => {
  const testService = createTestService();
  testService.loadTest('1');
  await when(() => !!testService.test);
  return testService;
};

describe('TestService', () => {
  it('is empty by default', () => {
    const testService = createTestService();
    expect(testService.test).toBeFalsy();
  });

  it('can load a test using api', async () => {
    const testService = await prepareTestService('1');

    assert(testService.test);
    expect(testService.test.questions.length).toBe(3);
    expect(testService.firstQuestionId === '21').toBeTruthy();
    expect(testService.lastQuestionId === '23').toBeTruthy();
  });

  it('can edit questions', async () => {
    const testService = await prepareTestService('1');

    expect(testService.test).toBeTruthy();
    assert(testService.test);
    expect(testService.test.questions[0].title).not.toEqual('New title');
    const questionId = testService.test.questions[0].id;
    testService.editQuestion(questionId, 'title', 'New title');
    expect(testService.test.questions[0].title).toEqual('New title');
  });

  it('can remove questions', async () => {
    const testService = await prepareTestService('1');

    expect(testService.test).toBeTruthy();
    assert(testService.test);
    expect(testService.getQuestion('21')).toBeTruthy();
    testService.removeQuestion('21');
    expect(testService.test.questions.find(question => question.id === '21')).toBeFalsy();
  });

  it('can duplicate questions', async () => {
    const testService = await prepareTestService('1');

    expect(testService.test).toBeTruthy();
    assert(testService.test);
    const questions = testService.test.questions;
    const title = testService.getQuestion('21').title;

    expect(questions.length).toBe(3);
    expect(questions.filter(question => question.title === title).length).toBe(1);

    testService.duplicateQuestion('21');

    expect(questions.length).toBe(4);
    expect(questions.filter(question => question.title === title).length).toBe(2);
  });

  it('can edit answerVariants', async () => {
    const testService = await prepareTestService('1');

    assert(testService.test);
    const question = testService.test.questions[2];
    if (question.type !== 'multiple') {
      throw new Error('Expected multiple type');
    }

    expect(question.answerVariants[0].isCorrect).toBeFalsy();
    expect(question.answerVariants[1].isCorrect).toBeFalsy();
    expect(question.answerVariants[2].isCorrect).toBeTruthy();
    testService.editVariant(question.id, question.answerVariants[1].id, 'isCorrect', true);
    expect(question.answerVariants[0].isCorrect).toBeFalsy();
    expect(question.answerVariants[1].isCorrect).toBeTruthy();
    expect(question.answerVariants[2].isCorrect).toBeTruthy();
  });

  it('can switch question type', async () => {
    const testService = await prepareTestService('1');

    testService.addEmptyQuestion();
    assert(testService.test);
    const question = testService.test.questions[3];
    expect(question.type).toBe('single');
    expect(question.answerVariants.length).toBe(0);

    testService.switchQuestionType(question.id, 'multiple');
    expect(question.type).toBe('multiple');
    expect(question.answerVariants.length).toBe(1);

    testService.switchQuestionType(question.id, 'single');
    testService.switchQuestionType(question.id, 'multiple');
    expect(question.answerVariants.length).toBe(1);
  });

  it('should show all questions as expanded by default', async () => {
    const testService = await prepareTestService('1');

    expect(testService.isExpanded('21')).toBeTruthy();
  });

  it('should collapse a question', async () => {
    const testService = await prepareTestService('1');

    expect(testService.isExpanded('21')).toBeTruthy();
    testService.toggleExpanded('21');
    expect(testService.isExpanded('21')).toBeFalsy();
    expect(testService.isExpanded('22')).toBeTruthy();
    expect(testService.isExpanded('23')).toBeTruthy();
  });

  it('should reorder questions', async () => {
    const testService = await prepareTestService('1');

    expect(testService.firstQuestionId).toBe('21');
    expect(testService.lastQuestionId).toBe('23');
    testService.moveQuestionFromTo(0, 1);
    expect(testService.firstQuestionId).toBe('22');
    expect(testService.lastQuestionId).toBe('23');
    testService.moveQuestionFromTo(1, 0);
    expect(testService.firstQuestionId).toBe('21');
    expect(testService.lastQuestionId).toBe('23');
  });

  it('should reorder variants', async () => {
    const testService = await prepareTestService('1');

    const question = testService.getQuestion('23');
    expect(question.answerVariants[0].id).toEqual('31');
    expect(question.answerVariants[1].id).toEqual('32');
    expect(question.answerVariants[2].id).toEqual('33');
    testService.moveVariantFromTo(question.id, 1, 0);
    expect(question.answerVariants[0].id).toEqual('32');
    expect(question.answerVariants[1].id).toEqual('31');
    expect(question.answerVariants[2].id).toEqual('33');
  });

  it('should collapse and expand questions', async () => {
    const testService = await prepareTestService('1');

    testService.collapseAll();
    expect(testService.isExpanded('21')).toBeFalsy();
    expect(testService.isExpanded('22')).toBeFalsy();
    expect(testService.isExpanded('23')).toBeFalsy();
    testService.expandAll();
    expect(testService.isExpanded('21')).toBeTruthy();
    expect(testService.isExpanded('22')).toBeTruthy();
    expect(testService.isExpanded('23')).toBeTruthy();
  });

  it('shows test validation errors', async () => {
    const testService = await prepareTestService('1');

    let errors = testService.testErrors;
    autorun(() => (errors = testService.testErrors));
    expect(errors).toEqual({});
    testService.editTest('title', '');
    expect(errors.title).toHaveLength(1);
  });

  it('shows question validation errors', async () => {
    const testService = await prepareTestService('1');

    let errors = testService.questionErrors('21');
    autorun(() => (errors = testService.questionErrors('21')));
    expect(errors).toEqual({});
    testService.editQuestion('21', 'title', '');
    expect(errors.title).toHaveLength(1);
  });
});
