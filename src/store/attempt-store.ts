import { action, computed, observable } from 'mobx';
import { AttemptApi, AttemptWithTest } from '../api/attempt-api';
import { Id } from '../model/id';
import { TestApi } from '../api/test-api';
import { AuthStore } from './auth-store';
import { isAnswerCorrect, Question } from '../model/question';
import { Answer, createAnswer } from '../model/answer';
import { firestore } from 'firebase/app';
import { RouterStore } from 'mobx-react-router';
import { assert } from 'ts-essentials';

export type CurrentAnswerStatus = 'empty' | 'edited' | 'given';

export class AttemptStore {
  @observable attempt?: AttemptWithTest;
  @observable isAttemptLoading = false;
  @observable questionIndex = 0;
  @observable currentAnswerVariants: Id[] = [];
  @observable currentAnswerText = '';
  @observable isFinishing = false;

  constructor(
    private attemptApi: AttemptApi,
    private testApi: TestApi,
    private authService: AuthStore,
    private routerStore: RouterStore
  ) {}

  @action startAttempt(testId: Id): void {
    this.isAttemptLoading = true;
    const user = this.authService.currentUser;
    this.attemptApi
      .start(testId, user.id)
      .then((attempt: AttemptWithTest) => {
        this.attempt = attempt;
        this.questionIndex = this.attempt.answers.length;
      })
      .finally(() => (this.isAttemptLoading = false));
  }

  @action toggleCurrentAnswerVariant(variantId: Id): void {
    if (this.currentQuestionAnswer) {
      return;
    }
    const answerVariantId = this.currentAnswerVariants.findIndex(id => id === variantId);
    if (answerVariantId === -1) {
      this.currentAnswerVariants.push(variantId);
    } else {
      this.currentAnswerVariants.splice(answerVariantId, 1);
    }
  }

  isVariantSelected(variantId: Id): boolean {
    return this.currentAnswerVariants.includes(variantId);
  }

  @action editCurrentAnswerText(text: string): void {
    this.currentAnswerText = text;
  }

  @action editCurrentAnswerVariants(variants: Id[]): void {
    this.currentAnswerVariants = variants;
  }

  @computed get currentAnswerStatus(): CurrentAnswerStatus {
    const answer = this.currentQuestionAnswer;
    if (answer) {
      return 'given';
    }
    return this.currentAnswerVariants.length || this.currentAnswerText ? 'edited' : 'empty';
  }

  @action next(): void {
    if (this.attempt && this.currentQuestionAnswer) {
      this.currentAnswerText = '';
      this.currentAnswerVariants = [];
      this.nextQuestion();
      this.attemptApi.save(this.attempt);
      return;
    }

    if (this.currentQuestion.type === 'multiple') {
      this.giveAnswer(this.currentQuestion, this.currentAnswerVariants);
    } else {
      this.giveAnswer(this.currentQuestion, this.currentAnswerText);
    }
  }

  @action nextQuestion(): void {
    assert(this.attempt);
    if (this.questionIndex < this.attempt.test.questions.length - 1) {
      this.questionIndex++;
    }
  }

  @action prevQuestion(): void {
    assert(this.attempt);
    if (this.questionIndex !== 0) {
      this.questionIndex--;
    }
  }

  @action private giveAnswer(question: Question, answerOrVariant: string | Id[]): void {
    assert(this.attempt);
    const isCorrect = isAnswerCorrect(question, answerOrVariant);

    const answer = this.attempt.answers.find(a => a.questionId === question.id);
    if (answer) {
      answer.answer = answerOrVariant;
      answer.isCorrect = isCorrect;
    } else {
      this.attempt.answers.push(createAnswer(question.id, answerOrVariant, isCorrect));
    }

    if (this.isCompleted) {
      this.isFinishing = true;
      this.attempt.finishedAt = firestore.Timestamp.now();
      this.attemptApi
        .save(this.attempt)
        .then(() => this.routerStore.push('/tests'))
        .finally(() => (this.isFinishing = false));
    }
  }

  @computed get currentQuestion(): Question {
    assert(this.attempt);
    return this.attempt.test.questions[this.questionIndex];
  }

  @computed get isPrevDisabled(): boolean {
    return this.questionIndex === 0;
  }

  @computed get completionPercent(): number {
    if (!this.attempt) {
      return 0;
    }
    return (this.questionIndex / this.attempt.test.questions.length) * 100;
  }

  @computed get currentQuestionAnswer(): Answer | null {
    if (!this.attempt) {
      return null;
    }
    const answers = this.attempt.answers.filter(answer => {
      return answer.questionId === this.currentQuestion.id;
    });
    if (answers.length > 1) {
      throw new Error('Should not be reached');
    }
    return answers.length ? answers[0] : null;
  }

  @computed get isCompleted(): boolean {
    if (!this.attempt) {
      return false;
    }
    return this.attempt.test.questions.length === this.attempt.answers.length;
  }
}
