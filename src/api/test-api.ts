import { Id } from '../model/id';
import { Test } from '../model/test';

export class TestApi {
  getOne(testId: Id): Promise<Test> {
    return 1 as any;
  }

  save(test: Test): Promise<unknown> {
    return 1 as any;
  }

  remove(testId: Id): Promise<unknown> {
    return 1 as any;
  }
}
