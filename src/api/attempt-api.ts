import { Attempt } from '../model/attempt';
import { Test } from '../model/test';
import { Id } from '../model/id';

export type AttemptWithTest = Attempt & {
  test: Test;
};

export class AttemptApi {
  start(testId: Id, userId: Id): Promise<AttemptWithTest> {
    return Promise.resolve() as any;
  }

  save(attempt: Attempt): Promise<void> {
    return 1 as any;
  }
}
