import React from 'react';
import { Collapsible } from './collapsible';

export default {
  title: 'Collapsible',
  component: Collapsible
};

const styles = {
  flex: 1,
  padding: '1rem'
};

export const Default = () => (
  <Collapsible
    collapsibleBody={() => (
      <div style={{ ...styles, backgroundColor: '#ccc' }}>Body</div>
    )}
    collapsibleToggle={() => (
      <div style={{ ...styles, backgroundColor: '#fff' }}>Toggle</div>
    )}
  />
);

export const DropUp = () => (
  <Collapsible
    isDropUp={true}
    collapsibleBody={() => (
      <div style={{ ...styles, backgroundColor: '#ccc' }}>Body</div>
    )}
    collapsibleToggle={() => (
      <div style={{ ...styles, backgroundColor: '#fff' }}>Toggle</div>
    )}
  />
);
