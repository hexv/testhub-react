import React, { ReactNode } from 'react';
import styles from './collapsible.module.scss';
import { animated, useSpring } from 'react-spring';
import { useToggle } from '../../lib/hooks/use-toggle';
import cn from 'classnames';

type Props = {
  collapsibleToggle: (isOn: boolean) => ReactNode;
  collapsibleBody: () => ReactNode;
  isDropUp?: boolean;
};

export const Collapsible = (props: Props) => {
  const [isOn, toggle] = useToggle();
  const animatedProps = useSpring({ maxHeight: isOn ? '100vh' : '0px' });

  return (
    <div
      className={cn(styles.accordionWrapper, {
        [styles.dropUp]: props.isDropUp
      })}
    >
      <button type='button' className={styles.accordionToggle} onClick={toggle}>
        {props.collapsibleToggle(isOn)}
      </button>
      <animated.div className={styles.accordionBody} style={animatedProps}>
        {props.collapsibleBody()}
      </animated.div>
    </div>
  );
};
