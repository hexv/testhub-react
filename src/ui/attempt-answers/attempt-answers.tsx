import React from 'react';
import { Accordion } from '../accordion/accordion';
import { Question } from '../../model/question';
import { Answer } from '../../model/answer';
import styles from './attempt-answers.module.scss';
import cn from 'classnames';

type Props = {
  questions: Array<Question & { answer?: Answer }>;
};

export const AttemptAnswers = (props: Props) => {
  const { questions } = props;

  return (
    <Accordion
      items={questions}
      renderHeader={question => (
        <div className={styles.questionTitle}>
          <span>{question.title}</span>
          <span
            className={cn(styles.questionStatus, {
              [styles.correct]: question.answer?.isCorrect,
              [styles.incorrect]: !question.answer?.isCorrect
            })}
          >
            {question.answer?.isCorrect ? 'Правильно' : 'Ошибка'}
          </span>
        </div>
      )}
      renderContent={question => {
        if (question.type === 'single') {
          return question.answerText;
        }

        if (question.type === 'multiple') {
          return (
            <ul>
              {question.answerVariants.map(variant => (
                <li key={variant.id}>{variant.text}</li>
              ))}
            </ul>
          );
        }
      }}
    />
  );
};
