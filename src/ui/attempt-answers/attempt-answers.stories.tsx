import React from 'react';
import { AttemptAnswers } from './attempt-answers';
import { StorybookContainer } from '../../../.storybook/storybook-container';
import { createMockAttempt } from '../../shared/mock/create-mock-attempt';

export default {
  title: 'AttemptAnswers',
  component: AttemptAnswers
};

const { test, attempt } = createMockAttempt();

const questions = test.questions.map(question => {
  const answer = attempt.answers.find(({ questionId }) => questionId === question.id);
  return {
    ...question,
    answer
  };
});

export const Default = () => (
  <StorybookContainer>
    <AttemptAnswers questions={questions} />
  </StorybookContainer>
);
