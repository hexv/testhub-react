import React from 'react';
import { Card } from './card';
import { StorybookContainer } from '../../../.storybook/storybook-container';

export default {
  title: 'Card',
  component: Card
};

export const Default = () => (
  <StorybookContainer>
    <Card />
  </StorybookContainer>
);
