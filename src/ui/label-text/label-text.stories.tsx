import React from 'react';
import { LabelText } from './label-text';
import { StorybookContainer } from '../../../.storybook/storybook-container';
import { Card } from '../card/card';

export default {
  title: 'LabelText',
  component: LabelText
};

export const Default = () => (
  <StorybookContainer>
    <LabelText text={'Quantitative section'}>
      <Card />
    </LabelText>
  </StorybookContainer>
);
