import React, { ReactNode } from 'react';
import styles from './label-text.module.scss';

type Props = {
  text: string;
  children: ReactNode;
};

export const LabelText = (props: Props) => {
  return (
    <div className={styles.labelWrapper}>
      <span className={styles.labelText}>{props.text}</span>
      {props.children}
    </div>
  );
};
