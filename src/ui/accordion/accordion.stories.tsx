import React from 'react';
import { Accordion } from './accordion';
import { StorybookContainer } from '../../../.storybook/storybook-container';

export default {
  title: 'Accordion',
  component: Accordion
};

const items = [{ title: '1' }, { title: '2' }, { title: '3' }];

const sharedProps = {
  renderHeader: (item: { title: string }) => `Header ${item.title}`,
  renderContent: (item: { title: string }) => `Body - ${item.title}`
};

export const Default = () => (
  <StorybookContainer>
    <Accordion items={items} {...sharedProps} />
  </StorybookContainer>
);

export const SingleValue = () => (
  <StorybookContainer>
    <Accordion items={[items[0]]} {...sharedProps} />
  </StorybookContainer>
);
