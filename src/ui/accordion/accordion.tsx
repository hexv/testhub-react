import React, { ReactNode } from 'react';
import { Collapsible } from '../collapsible/collapsible';
import styles from './accordion.module.scss';
import cn from 'classnames';
import { ArrowIcon, ArrowIconDirection } from './arrow-icon';

const getIconDirection = (
  isOn: boolean,
  isLast: boolean,
  singleItem: boolean
): ArrowIconDirection => {
  if (isOn && isLast && !singleItem) {
    return 'top';
  }
  return isOn ? 'bottom' : 'right';
};

type Props<T extends object> = {
  items: T[];
  renderHeader: (item: T) => ReactNode;
  renderContent: (item: T) => ReactNode;
};

export const Accordion = <T extends object>(props: Props<T>) => {
  const { items, renderContent, renderHeader } = props;
  return (
    <div className={styles.list}>
      {items.map((item, i) => {
        const isLast = i === items.length - 1;
        const singleItem = items.length === 1;
        return (
          <Collapsible
            key={i}
            isDropUp={isLast && !singleItem}
            collapsibleToggle={isOn => (
              <div
                className={cn(styles.listItem, {
                  [styles.roundedTop]: i === 0,
                  [styles.roundedBottom]: isLast && !singleItem
                })}
              >
                <i className={styles.toggleIconWrapper}>
                  <ArrowIcon direction={getIconDirection(isOn, isLast, singleItem)} />
                </i>
                {renderHeader(item)}
              </div>
            )}
            collapsibleBody={() => (
              <div
                className={cn(styles.listSubItem, {
                  [styles.roundedBottom]: singleItem
                })}
              >
                {renderContent(item)}
              </div>
            )}
          />
        );
      })}
    </div>
  );
};
