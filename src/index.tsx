import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.scss';
import { App } from './app';
import { StoreContext } from './store/use-store';
import { createStores } from './store/create-stores';
import { enableLogging } from 'mobx-logger';

const stores = createStores();
enableLogging();

const render = () => (
  <StoreContext.Provider value={stores}>
    <App />
  </StoreContext.Provider>
);

ReactDOM.render(render(), document.getElementById('root'));

if (module['hot']) {
  module['hot'].accept('./store/create-stores', render);
}
