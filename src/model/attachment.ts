import { HasId } from './id';

export type Attachment = HasId & {
  src: string;
  description?: string;
};
