import { id, HasId } from './id';
import { Sortable } from '../lib/sorting/sorting';

export type Variant = (HasId & Sortable) & {
  text: string;
  isCorrect: boolean;
};

export const createEmptyVariant = (orderIndex: number): Variant => {
  return {
    id: id(),
    text: '',
    isCorrect: false,
    orderIndex
  };
};
