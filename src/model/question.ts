import { Variant } from './variant';
import { id, HasId, Id } from './id';
import { equalIgnoreOrder } from '../lib/equal-ignore-order';
import { Sortable } from '../lib/sorting/sorting';

export type QuestionType = 'multiple' | 'single';

export type Question = (HasId & Sortable) & {
  title: string;
  type: QuestionType;
  answerVariants: Variant[];
  answerText: string;
};

export const createEmptyQuestion = (orderIndex: number): Question => {
  return {
    id: id(),
    title: '',
    type: 'single',
    answerVariants: [],
    answerText: '',
    orderIndex
  };
};

export const createQuestion = (props: Omit<Question, 'id'>): Question => {
  return {
    id: id(),
    ...props
  }
};

export const isAnswerCorrect = (question: Question, answerOrVariant: string | Id[]): boolean => {
  switch (question.type) {
    case 'single':
      return question.answerText === answerOrVariant;
    case 'multiple':
      if (!Array.isArray(answerOrVariant)) {
        throw new Error('Expected an array');
      }

      const correctVariantIds = question.answerVariants
        .filter(variant => variant.isCorrect)
        .map(variant => variant.id);

      return equalIgnoreOrder(correctVariantIds, answerOrVariant);
    default:
      throw new Error('Invalid question type');
  }
};

export const validateQuestion = (question: Question): Record<string, string[]> => {
  const errors: Record<string, string[]> = {};

  if (!question.title) {
    errors.title = ['Title is required'];
  }

  return errors;
};
