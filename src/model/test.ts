import { createEmptyQuestion, Question } from './question';
import { id, HasId } from './id';
import { firestore } from 'firebase/app';

export type Statistics = {
  averageCompletionTime: number; // Seconds
  finishedAttemptCount: number;
  totalAttemptCount: number;
};

export type Test = HasId & {
  title: string;
  description: string;
  tags: string[];
  configShowCorrectAnswer: boolean;
  configShuffleQuestionsAnswers: boolean;
  configIsPrivate: boolean;
  questions: Question[];
  createdAt: firestore.Timestamp;
  statistics?: Readonly<Statistics>;
};

export const createEmptyTest = (): Test => {
  return {
    id: id(),
    createdAt: firestore.Timestamp.now(),
    title: '',
    description: '',
    tags: [],
    configShowCorrectAnswer: true,
    configShuffleQuestionsAnswers: false,
    configIsPrivate: false,
    questions: [createEmptyQuestion(0)],
    statistics: {
      averageCompletionTime: 0,
      finishedAttemptCount: 0,
      totalAttemptCount: 0
    }
  };
};

export const validateTest = (test: Test): Record<string, string[]> => {
  const errors: Record<string, string[]> = {};
  if (!test.title) {
    errors.title = ['Title is required'];
  }
  return errors;
};
