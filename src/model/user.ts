import { HasId } from './id';

export type User = HasId & {
  email: string;
};
