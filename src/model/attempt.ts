import { Answer } from './answer';
import { HasId, id, Id } from './id';
import { firestore } from 'firebase/app';
import { Merge } from 'ts-essentials';

export type Attempt = HasId & {
  createdAt: firestore.Timestamp;
  finishedAt: firestore.Timestamp | null;
  testId: Id;
  answers: Answer[];
};

export const createAttempt = (testId: Id, answers: Answer[] = []): Attempt => {
  return {
    id: id(),
    createdAt: firestore.Timestamp.now(),
    finishedAt: null,
    testId: testId,
    answers
  };
};

export type FirestoreAttempt = Merge<
  Attempt,
  {
    testId: firestore.DocumentReference;
  }
> & {
  userId: firestore.DocumentReference;
};
