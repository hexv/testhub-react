import { id, HasId, Id } from './id';

export type Answer = HasId & {
  questionId: Id;
  answer: string | Id[];
  isCorrect: boolean;
};

export const createAnswer = (questionId: Id, answer: string | Id[], isCorrect: boolean): Answer => {
  return {
    id: id(),
    questionId,
    answer,
    isCorrect
  };
};
