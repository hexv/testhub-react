import nanoid from 'nanoid';

export type Id = string;

export type HasId = {
  id: Id;
};

// Make IDE autoimport 'id' function call
export const id = (): Id => {
  return nanoid();
};
